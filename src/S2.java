import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

    public class S2 {
        private static final String fisier="fisier.txt";
        public static void main(String[] args) {
            JFrame frame=new JFrame();
            frame.setSize(200,200);
            frame.getContentPane().setLayout(new FlowLayout());
            JTextField text=new JTextField("Text");
            JButton buton=new JButton("Scrie text");;
            buton.addActionListener(e -> {
                File file = new File(fisier);
                try {
                    try (FileWriter writer = new FileWriter(file)) {
                        writer.write(text.getText());
                    }
                } catch (IOException | HeadlessException z) {
                    JOptionPane.showMessageDialog(null, e);
                }

            });
            frame.add(text);
            frame.add(buton);
            frame.setVisible(true);
        }
    }



