public class A {
    public A() {
    }
}

class B extends A {
    private String param;
    private C c;
    public B() {
        c=new C();
    }
}

class C {
    public C() {
    }
}

class D {
    public D() {
    }
    public void f() {
    }
}

class E {
    public E() {
    }
}

class Z {
    public Z() {
    }
    public void g() {
    }

}